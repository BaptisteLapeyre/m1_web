const fs = require('fs');
const http = require('http');
const pug = require('pug');
var bodyParser = require('body-parser')

const express = require('express');
const app = express();

const path = require('path');
const { nextTick, exit } = require('process');
const { v4: uuidv4 } = require('uuid');

const mongoose = require("mongoose")

mongoose.connect("mongodb://localhost/TP_Web", {useNewUrlParser: true});

const db = mongoose.connection
const citieSchema = new mongoose.Schema({id:String, name:String})
const Citie = mongoose.model('Citie', citieSchema)

var methodOverride = require("method-override");
app.use(methodOverride("_method"))

const port = 3000;

app.use(express.urlencoded({
    extended: true
  }))

var myArgs = process.argv[2];
if (!myArgs) {
    myArgs = "data.csv"
}

var data = fs.readFileSync('./' + myArgs,'utf8')

data = data.split('\n')
var resu = []
data.forEach(element => {
    var split = element.split(';')
    resu.push({id:split[0], ville: split[1]})
});
const compiledFunction = pug.compileFile('template.pug');


app.get('/cities', (req, res, next) => {
    Citie.find(function (err, cities) {
        if (err) return console.error(err)
        const generatedTemplate = compiledFunction({
            data: cities,
        })
        res.send(generatedTemplate)
    })
})


app.put('/city/:id', bodyParser.json(), (req, res, next) => {
    const result = Citie.updateOne({ id: req.params.id }, { name: req.body.ville }, function (err, ressult) {
        if (ressult.nModified == 0) {
            res.status(500).send("l'id de la ville n'existe pas")
            return
        }
        res.statusCode = 200
        res.send("ville modifié")
    });

    
    
})

app.delete('/city/:id', (req, res, next) => {
    
    Citie.deleteOne({ id: req.params.id }, function (err, mongooseDeleteResult) {
        if (err) return console.error(err)
        if (mongooseDeleteResult.deletedCount = 0) {
            res.status(500).send("l'id de la ville n'existe pas")
            return
        }
        res.statusCode = 200
        res.send("ville supprimé")
    });
})

app.post('/city', bodyParser.json(), (req, res, next) => {
    Citie.find({ name:req.body.ville }, function(err, arr) {
        if (arr.length !== 0) {
            res.status(500).send("la ville existe deja")
            return 
        }else{
            const ville = new Citie({id:uuidv4(), name:req.body.ville})

            ville.save(function (err, result) {
                if (err) {
                    res.status(500).send("impossible d'ajouter la ville")
                    return console.error(err)
                }
            })
        
            // res.statusCode = 200
            // res.send("ville ajoute")
            res.redirect("/cities")
        }
    });

    
    
})


const compiledAddCity = pug.compileFile('templateAddCity.pug');

app.get('/addCity', (req, res, next) => {
    const generatedTemplate = compiledAddCity()
    res.send(generatedTemplate)
})

app.use(express.static(path.join(__dirname, 'image')))

app.listen(port, () => {
    console.log('serveur running at port ${port}');
})

