const fs = require('fs');
const http = require('http');
const pug = require('pug');
var bodyParser = require('body-parser')

const express = require('express');
const app = express();

const path = require('path');
const { nextTick, exit } = require('process');
const { v4: uuidv4 } = require('uuid');


const port = 3000;



var myArgs = process.argv[2];
if (!myArgs) {
    myArgs = "data.csv"
}

var data = fs.readFileSync('./' + myArgs,'utf8')

data = data.split('\n')
var resu = []
data.forEach(element => {
    var split = element.split(';')
    resu.push({id:split[0], ville: split[1]})
});
console.log(resu)
const compiledFunction = pug.compileFile('template.pug');

// const serv = http.createServer(
//     (req, res) => {

//         const generatedTemplate = compiledFunction({
//             data: resu,
//         })

//         res.statusCode = 200
//         res.setHeader('content-type', 'text/html')
//         res.end(generatedTemplate)
//     }
// )

app.get('/cities', (req, res, next) => {
    fs.readFile('cities/cities.json', function (err, data) {
        if (err) {
            next(err)
        }else{
            data = JSON.parse(data)
            const generatedTemplate = compiledFunction({
                data: data.cities,
            })
            res.send(generatedTemplate)
        }
    })
})

app.get('/', (req, res) => {
    const generatedTemplate = compiledFunction({
        data: resu,
    })

    res.statusCode = 200
    res.setHeader('content-type', 'text/html')
    res.send(generatedTemplate)
})

let buffer = new Buffer.from('{"cities": []}')

app.put('/city/:id', bodyParser.json(), (req, res, next) => {
    
    console.log(req.body.ville)
    fs.readFile('cities/cities.json', function (err, data) {
        if (err) {
            next(err)
        }else{
            console.log(JSON.parse(data).cities[0])
            let exist = false
            data = JSON.parse(data)
            data.cities.forEach(citie => {
                if(citie.id == req.params.id){
                    citie.name = req.body.ville
                    exist = true
                    return
                }
            });
            if (!exist) {
                res.status(500).send("l'id de la ville n'existe pas")
                return
            }
            fs.writeFile('cities/cities.json', JSON.stringify(data),  (err) => {
                if (err) throw err;
              })
            res.statusCode = 200
            res.send("ville modifié")
        }
    })
    
})

app.delete('/city/:id', (req, res, next) => {
    
    fs.readFile('cities/cities.json', function (err, data) {
        if (err) {
            next(err)
        }else{
            console.log(JSON.parse(data).cities[0])
            data = JSON.parse(data)

            const newCities = data.cities.filter(citie => citie.id != req.params.id);
            console.log(newCities)
            if (newCities.length == data.cities.length) {
                res.status(500).send("l'id de la ville n'existe pas")
                return
            }

            data.cities = newCities
            fs.writeFile('cities/cities.json', JSON.stringify(data),  (err) => {
                if (err) throw err;
              })
            res.statusCode = 200
            res.send("ville supprimé")
        }
    })
    
})

app.post('/city', bodyParser.json(), (req, res, next) => {
    fs.open('cities/cities.json', 'wx', function (err, file) {
        if (err) {
            
        }else{
            fs.write(
                file, buffer, 0, buffer.length, null, 
                 (err, bytes, buffer) => {
                }
            )
        }
        
    });

    console.log(req.body.ville)
    fs.readFile('cities/cities.json', function (err, data) {
        if (err) {
            next(err)
        }else{
            console.log(JSON.parse(data).cities[0])
            let exist = false
            data = JSON.parse(data)
            data.cities.forEach(citie => {
                if(citie.name == req.body.ville){
                    res.status(500).send("ville existe deja")
                    exist = true
                    return
                }
            });
            if (exist) {
                return
            }

            data.cities.push({id:uuidv4(),name:req.body.ville})
            fs.writeFile('cities/cities.json', JSON.stringify(data),  (err) => {
                if (err) throw err;
              })
            res.statusCode = 200
            res.send("ville ajoute")
        }
    })
    
})

app.use(express.static(path.join(__dirname, 'image')))

app.listen(port, () => {
    console.log('serveur running at port ${port}');
})

